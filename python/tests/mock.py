# external imports
from hexathon import add_0x
from chainlib.eth.tx import TxFactory
from chainlib.eth.contract import (
        ABIContractEncoder,
        ABIContractType,
        )
from chainlib.hash import keccak256_string_to_hex

dummy_add_signature = method = keccak256_string_to_hex('add(address)')[:8]

class DummyAccountsIndex(TxFactory):
    
    def add(self, contract_address, address, sender_address):
        enc = ABIContractEncoder()
        enc.method('add')
        enc.typ(ABIContractType.ADDRESS)
        enc.address(address)
        data = enc.get()
        tx = self.template(sender_address, contract_address, use_nonce=True)
        tx = self.set_code(tx, data)
        return self.build(tx)
