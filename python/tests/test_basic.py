# standard imports
import logging
import os
import unittest

# third-party imports
from chainlib.eth.contract import (
    ABIContractEncoder,
    ABIContractType,
)
from eth_erc20 import ERC20
from chainlib.hash import strip_0x
from chainlib.eth.nonce import RPCNonceOracle
from chainlib.eth.tx import (
    TxFactory,
    TxFormat,
    receipt,
    unpack,
    Tx,
)
from chainlib.eth.block import block_latest
from erc20_faucet import Faucet
from hexathon import (
        add_0x,
        strip_0x,
        )

# local imports
from tests.base import FaucetTestRealStore
from sarafu_faucet import MinterFaucet

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()


class Test(FaucetTestRealStore):

    def test_set_amount(self):
        nonce_oracle = RPCNonceOracle(self.accounts[1], self.rpc)
        c = MinterFaucet(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.set_amount(self.address, self.accounts[1], 100 * (10 ** 6))
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)


    def test_not_in_accounts_index(self):
        trudy_address = add_0x(os.urandom(20).hex())
        nonce_oracle = RPCNonceOracle(self.accounts[0], self.rpc)
        c = MinterFaucet(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.give_to(self.address, self.accounts[0], trudy_address)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 0)


    def test_in_accounts_index(self):
        nonce_oracle = RPCNonceOracle(self.accounts[0], self.rpc)
        txf = TxFactory(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        tx = txf.template(self.accounts[0], self.dummy_accounts_index, use_nonce=True)
        enc = ABIContractEncoder()
        enc.method('add')
        enc.typ(ABIContractType.ADDRESS)
        enc.address(self.accounts[2])
        code = enc.get()
        tx = txf.set_code(tx, code)
        (tx_hash_hex, o) = txf.build(tx)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        gift_value = 100 * (10 ** 6)
        nonce_oracle = RPCNonceOracle(self.accounts[1], self.rpc)
        c = MinterFaucet(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.set_amount(self.address, self.accounts[1], gift_value)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[0], self.rpc)
        c = MinterFaucet(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.give_to(self.address, self.accounts[0], self.accounts[2])
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        c = ERC20(self.chain_spec)
        o = c.balance_of(self.token_address, self.accounts[2], sender_address=self.accounts[0])
        r = self.rpc.do(o)
        self.assertEqual(c.parse_balance(r), gift_value)


    def test_give_twice(self):
        nonce_oracle = RPCNonceOracle(self.accounts[0], self.rpc)
        txf = TxFactory(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        tx = txf.template(self.accounts[0], self.dummy_accounts_index, use_nonce=True)
        enc = ABIContractEncoder()
        enc.method('add')
        enc.typ(ABIContractType.ADDRESS)
        enc.address(self.accounts[1])
        code = enc.get()
        tx = txf.set_code(tx, code)
        (tx_hash_hex, o) = txf.build(tx)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        nonce_oracle = RPCNonceOracle(self.accounts[0], self.rpc)
        c = MinterFaucet(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.give_to(self.address, self.accounts[0], self.accounts[1])
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        (tx_hash_hex, o) = c.give_to(self.address, self.accounts[0], self.accounts[1])
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 0)


    def test_cooldown(self):
        nonce_oracle = RPCNonceOracle(self.accounts[0], self.rpc)
        c = Faucet(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        o = c.usable_for(self.address, self.accounts[1], sender_address=self.accounts[0])
        r = self.rpc.do(o)
        usable = c.parse_usable_for(r)
        self.assertTrue(usable)

        txf = TxFactory(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        tx = txf.template(self.accounts[0], self.dummy_accounts_index, use_nonce=True)
        enc = ABIContractEncoder()
        enc.method('add')
        enc.typ(ABIContractType.ADDRESS)
        enc.address(self.accounts[1])
        code = enc.get()
        tx = txf.set_code(tx, code)
        (tx_hash_hex, o) = txf.build(tx)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        c = MinterFaucet(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.give_to(self.address, self.accounts[0], self.accounts[1])
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        c = Faucet(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        o = c.usable_for(self.address, self.accounts[1], sender_address=self.accounts[0])
        r = self.rpc.do(o)
        usable = c.parse_usable_for(r)
        self.assertFalse(usable)


    def test_parse_give_to_request(self):
        nonce_oracle = RPCNonceOracle(self.accounts[0], self.rpc)
        faucet = MinterFaucet(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, tx_raw_hex) = faucet.give_to(self.address, self.accounts[0], self.accounts[1], tx_format=TxFormat.RLP_SIGNED)
        tx_dict = unpack(bytes.fromhex(strip_0x(tx_raw_hex)), self.chain_spec)
        tx = Tx(src=tx_dict)
        r = MinterFaucet.parse_give_to_request(tx.payload)
        self.assertEqual(strip_0x(self.accounts[1]), r[0])


    # TODO: Move this test to erc20_single_shot_faucet once the tests there has been ported from web3
    def test_token_query(self):
        faucet = Faucet(self.chain_spec)
        o = faucet.token(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        token = faucet.parse_token(r)
        self.assertEqual(strip_0x(self.token_address), token)



    def test_value_query(self):
        o = block_latest()
        r = self.rpc.do(o)
        block_height = int(r)

        faucet = Faucet(self.chain_spec)
        o = faucet.token_amount(self.address, height=block_height, sender_address=self.accounts[0])
        #o = faucet.token_amount(self.address, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        amount = faucet.parse_token_amount(r)
        self.assertEqual(amount, 0)

        set_amount = 200 * (10 ** 6)

        nonce_oracle = RPCNonceOracle(self.accounts[1], self.rpc)
        c = MinterFaucet(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.set_amount(self.address, self.accounts[1], set_amount)
        r = self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        logg.debug('rcpt {}'.format(r))
        self.assertEqual(r['status'], 1)

        self.backend.mine_blocks(2)

        o = block_latest()
        r = self.rpc.do(o)
        new_block_height = int(r)

        faucet = Faucet(self.chain_spec)
        o = faucet.token_amount(self.address, height=new_block_height, sender_address=self.accounts[0])
        r = self.rpc.do(o)
        amount = faucet.parse_token_amount(r)
        self.assertEqual(amount, set_amount)


if __name__ == '__main__':
    unittest.main()
