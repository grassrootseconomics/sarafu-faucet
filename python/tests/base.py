# standard imports
import logging
import os
import unittest

# third-party imports
from chainlib.eth.nonce import RPCNonceOracle
from chainlib.eth.tx import (
    TxFactory,
    receipt,
)
from chainlib.eth.unittest.ethtester import EthTesterCase
from erc20_faucet.faucet import SingleShotFaucet
from giftable_erc20_token import GiftableToken

# local imports
from sarafu_faucet import MinterFaucet

script_dir = os.path.realpath(os.path.dirname(__file__))

logg = logging.getLogger()


class FaucetTest(EthTesterCase):

    def setUp(self):
        super(FaucetTest, self).setUp()

        nonce_oracle = RPCNonceOracle(self.accounts[0], self.rpc)

        # dummy accounts index
        f = open(os.path.join(script_dir, 'testdata/evm/DummyAccountsIndex.bin'))
        code = f.read().rstrip()
        f.close()
        txf = TxFactory(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        tx = txf.template(self.accounts[0], None, use_nonce=True)
        tx = txf.set_code(tx, code)
        (tx_hash_hex, tx) = txf.build(tx)
        tx_hash = self.rpc.do(tx)
        o = receipt(tx_hash_hex)
        rcpt = self.rpc.do(o)
        self.assertEqual(rcpt['status'], 1)
        self.dummy_accounts_index = rcpt['contract_address']

        # dummy store
        tx = txf.template(self.accounts[0], None, use_nonce=True)
        tx = txf.set_code(tx, code)
        (tx_hash_hex, tx) = txf.build(tx)
        tx_hash = self.rpc.do(tx)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)
        dummy_faucet_store = r['contract_address']

        # dummy token
        c = GiftableToken(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.constructor(self.accounts[0], 'FooToken', 'FOO', 6)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)
        self.token_address = r['contract_address']


        c = MinterFaucet(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.constructor(self.accounts[0], self.accounts[1], self.token_address, dummy_faucet_store, self.dummy_accounts_index)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)
        self.address = r['contract_address']

        c = GiftableToken(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.add_minter(self.token_address, self.accounts[0], self.address)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        logg.info('faucet deployed with hash {}'.format(tx_hash_hex))


class FaucetTestRealStore(EthTesterCase):

    def setUp(self):
        super(FaucetTestRealStore, self).setUp()

        nonce_oracle = RPCNonceOracle(self.accounts[0], self.rpc)

        # dummy accounts index
        f = open(os.path.join(script_dir, 'testdata/evm/DummyAccountsIndex.bin'))
        code = f.read().rstrip()
        f.close()
        txf = TxFactory(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        tx = txf.template(self.accounts[0], None, use_nonce=True)
        tx = txf.set_code(tx, code)
        (tx_hash_hex, tx) = txf.build(tx)
        tx_hash = self.rpc.do(tx)
        o = receipt(tx_hash_hex)
        rcpt = self.rpc.do(o)
        self.assertEqual(rcpt['status'], 1)
        self.dummy_accounts_index = rcpt['contract_address']

        # real store
        code = SingleShotFaucet.bytecode(part='storage')
        c = TxFactory(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        tx = c.template(self.accounts[0], None, use_nonce=True)
        tx = c.set_code(tx, code)
        (tx_hash_hex, o) = c.build(tx)
        self.rpc.do(o)

        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)
        self.real_faucet_store = r['contract_address']

        # dummy token
        c = GiftableToken(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.constructor(self.accounts[0], 'FooToken', 'FOO', 6)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)
        self.token_address = r['contract_address']

        c = MinterFaucet(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.constructor(self.accounts[0], self.accounts[1], self.token_address, self.real_faucet_store, self.dummy_accounts_index)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)
        self.address = r['contract_address']

        c = GiftableToken(self.chain_spec, signer=self.signer, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.add_minter(self.token_address, self.accounts[0], self.address)
        self.rpc.do(o)
        o = receipt(tx_hash_hex)
        r = self.rpc.do(o)
        self.assertEqual(r['status'], 1)

        logg.info('faucet deployed with hash {}'.format(tx_hash_hex))
    
if __name__ == '__main__':
    unittest.main()
