# standard imports
import os
import logging
import tempfile

# external imports
import chainlib.eth.cli
from chainlib.eth.connection import EthHTTPConnection
from chainlib.eth.nonce import (
        RPCNonceOracle,
        OverrideNonceOracle,
        )
from chainlib.eth.gas import (
        RPCGasOracle,
        OverrideGasOracle,
        )
from chainlib.chain import ChainSpec

# local imports
from sarafu_faucet.factory import MinterFaucet


logging.basicConfig(level=logging.WARNING)
logg = logging.getLogger()

script_dir = os.path.dirname(os.path.realpath(__file__))

config_dir = os.path.join(script_dir, '..', 'data', 'config')

arg_flags = chainlib.eth.cli.argflag_std_write | chainlib.eth.cli.Flag.EXEC
argparser = chainlib.eth.cli.ArgumentParser(arg_flags)
argparser.add_positional('beneficiary', type=str, help='Address to send tokens to')
args = argparser.parse_args()
extra_args = {
    'beneficiary': None,
        }
#config = chainlib.eth.cli.Config.from_args(args, arg_flags, extra_args=extra_args, default_config_dir=config_dir)
config = chainlib.eth.cli.Config.from_args(args, arg_flags, extra_args=extra_args)

wallet = chainlib.eth.cli.Wallet()
wallet.from_config(config)

rpc = chainlib.eth.cli.Rpc(wallet=wallet)
conn = rpc.connect_by_config(config)

chain_spec = ChainSpec.from_chain_str(config.get('CHAIN_SPEC'))


def main():
    signer = rpc.get_signer()
    signer_address = rpc.get_sender_address()

    txf = MinterFaucet(chain_spec, signer=signer, gas_oracle=rpc.get_gas_oracle(), nonce_oracle=rpc.get_nonce_oracle())
    (tx_hash_hex, o) = txf.give_to(config.get('_EXEC_ADDRESS'), signer_address, config.get('_BENEFICIARY'))
    if config.get('_RPC_SEND'):
        r = conn.do(o)
        if config.get('_WAIT'):
            r = conn.wait(tx_hash_hex)
            if r['status'] == 0:
                sys.stderr.write('EVM revert. Wish I had more to tell you')
                sys.exit(1)

        logg.info('faucet gift to {} tx {}'.format(config.get('_RECIPIENT'), tx_hash_hex))

        print(tx_hash_hex)
    else:
        if config.get('_RAW'):
            print(o['params'][0])
        else:
            print(o)


if __name__ == '__main__':
    main()
