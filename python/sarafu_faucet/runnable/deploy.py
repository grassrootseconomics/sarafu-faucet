"""Deploys sarafu faucet contract

.. moduleauthor:: Louis Holbrook <dev@holbrook.no>
.. pgp:: 0826EDA1702D1E87C6E2875121D2E7BB88C2A746 

"""

# standard imports
import sys
import os
import json
import argparse
import logging

# external imports
import chainlib.eth.cli
from funga.eth.signer import EIP155Signer
from funga.eth.keystore.dict import DictKeystore
from chainlib.chain import ChainSpec
from chainlib.eth.nonce import (
        RPCNonceOracle,
        OverrideNonceOracle,
        )
from chainlib.eth.gas import (
        RPCGasOracle,
        OverrideGasOracle,
        )
from chainlib.eth.connection import EthHTTPConnection
from chainlib.eth.tx import (
        receipt,
        TxFactory,
        )
from chainlib.eth.constant import ZERO_ADDRESS
from erc20_faucet.faucet import SingleShotFaucet 

# local imports
from sarafu_faucet import MinterFaucet

logging.basicConfig(level=logging.WARNING)
logg = logging.getLogger()

script_dir = os.path.dirname(os.path.realpath(__file__))
config_dir = os.path.join(script_dir, '..', 'data', 'config')

arg_flags = chainlib.eth.cli.argflag_std_write
argparser = chainlib.eth.cli.ArgumentParser(arg_flags)
argparser.add_argument('--store-address', type=str, dest='store_address', help='Faucet address index store')
argparser.add_argument('--overrider-address', type=str, dest='overrider_address', help='Overrider address')
argparser.add_argument('--account-index-address', type=str, dest='account_index_address', default=ZERO_ADDRESS, help='Account index contract address')
argparser.add_argument('token_address', type=str, help='Mintable token address')
args = argparser.parse_args()
extra_args = {
    'overrider_address': None,
    'store_address': None,
    'account_index_address': None,
    'token_address': None,
        }
#config = chainlib.eth.cli.Config.from_args(args, arg_flags, extra_args=extra_args, default_config_dir=config_dir, default_fee_limit=MinterFaucet.gas())
config = chainlib.eth.cli.Config.from_args(args, arg_flags, extra_args=extra_args, default_fee_limit=MinterFaucet.gas())

wallet = chainlib.eth.cli.Wallet()
wallet.from_config(config)

rpc = chainlib.eth.cli.Rpc(wallet=wallet)
conn = rpc.connect_by_config(config)

chain_spec = ChainSpec.from_chain_str(config.get('CHAIN_SPEC'))


def main():
    signer = rpc.get_signer()
    signer_address = rpc.get_sender_address()

    gas_oracle = rpc.get_gas_oracle()
    nonce_oracle = rpc.get_nonce_oracle()

    overrider_address = config.get('_OVERRIDER_ADDRESS', signer_address)
    logg.info('overrider address will be set to {}'.format(signer_address))
    logg.info('account index address will be set to {}'.format(config.get('_ACCOUNT_INDEX_ADDRESS')))


    if config.get('_RPC_SEND'):
        store_address = config.get('_STORE_ADDRESS')
        if store_address == None:
            c = SingleShotFaucet(chain_spec, signer=signer, nonce_oracle=nonce_oracle, gas_oracle=gas_oracle)
            (tx_hash_hex, o) = c.store_constructor(signer_address)
            conn.do(o)
            r = conn.wait(tx_hash_hex)
            if r['status'] == 0:
                sys.stderr.write('EVM revert while deploying contract. Wish I had more to tell you')
                sys.exit(1)
            # TODO: pass through translator for keys (evm tester uses underscore instead of camelcase)
            store_address = r['contractAddress']
            logg.info('deployed faucet store on {}'.format(store_address))

        c = MinterFaucet(chain_spec, signer=signer, gas_oracle=gas_oracle, nonce_oracle=nonce_oracle)
        (tx_hash_hex, o) = c.constructor(signer_address, overrider_address, config.get('_TOKEN_ADDRESS'), store_address, config.get('_ACCOUNT_INDEX_ADDRESS'))
        conn.do(o)
        if config.get('_WAIT'):
            r = conn.wait(tx_hash_hex)
            if r['status'] == 0:
                sys.stderr.write('EVM revert while deploying contract. Wish I had more to tell you')
                sys.exit(1)
            # TODO: pass through translator for keys (evm tester uses underscore instead of camelcase)
            address = r['contractAddress']
            print(address)
        else:
            print(tx_hash_hex)
    else:
        o = None
        if config.get('_STORE_ADDRESS'):
            logg.info('creating constructor for faucet with store address {}'.format(config.get('_STORE_ADDRESS')))
            c = MinterFaucet(chain_spec, signer=signer, gas_oracle=gas_oracle, nonce_oracle=nonce_oracle)
            (tx_hash_hex, o) = c.constructor(signer_address, overrider_address, config.get('_TOKEN_ADDRESS'), config.get('_STORE_ADDRESS'), config.get('_ACCOUNT_INDEX_ADDRESS'))
        else:
            logg.info('creating constructor for faucet store')
            c = SingleShotFaucet(chain_spec, signer=signer, nonce_oracle=nonce_oracle, gas_oracle=gas_oracle)
            (tx_hash_hex, o) = c.store_constructor(signer_address)
        if config.get('_RAW'):
            print(o['params'][0])
        else:
            print(o)


if __name__ == '__main__':
    main()
