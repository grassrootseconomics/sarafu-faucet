# standard imports
import os
import logging

# external imports
from chainlib.eth.tx import (
        TxFactory,
        TxFormat,
        )
from chainlib.eth.error import RequestMismatchException
from hexathon import (
        strip_0x,
        add_0x,
        )
from chainlib.eth.contract import (
        ABIContractEncoder,
        ABIContractDecoder,
        ABIContractType,
        )
from chainlib.eth.error import RequestMismatchException

# local imports
from sarafu_faucet.data import data_dir

logg = logging.getLogger()

script_dir = os.path.realpath(os.path.dirname(__file__))
data_dir = os.path.join(script_dir, 'data')


class MinterFaucet(TxFactory):

    __abi = None
    __bytecode = None

    @staticmethod
    def bytecode():
        if MinterFaucet.__bytecode is None:
            f = open(os.path.join(data_dir, 'MinterFaucet.bin'))
            MinterFaucet.__bytecode = f.read()
            f.close()
        return MinterFaucet.__bytecode


    @staticmethod
    def gas(code=None):
        return 2300000


    def constructor(self, sender_address, overrider_address, minter_address, store_address, accounts_index_address):
        code = MinterFaucet.bytecode()
        tx = self.template(sender_address, None, use_nonce=True)
        enc = ABIContractEncoder()
        enc.address(overrider_address)
        enc.address(minter_address)
        enc.address(store_address)
        enc.address(accounts_index_address)
        data = enc.get()
        code += data
        tx = self.set_code(tx, code)
        return self.build(tx)


    def give_to(self, contract_address, sender_address, address, tx_format=TxFormat.JSONRPC):
        enc = ABIContractEncoder()
        enc.method('giveTo')
        enc.typ(ABIContractType.ADDRESS)
        enc.address(address)
        data = enc.get()
        tx = self.template(sender_address, contract_address, use_nonce=True)
        tx = self.set_code(tx, data)
        tx = self.finalize(tx, tx_format)
        return tx


    def set_amount(self, contract_address, sender_address, amount, tx_format=TxFormat.JSONRPC):
        enc = ABIContractEncoder()
        enc.method('setAmount')
        enc.typ(ABIContractType.UINT256)
        enc.uint256(amount)
        data = enc.get()
        tx = self.template(sender_address, contract_address, use_nonce=True)
        tx = self.set_code(tx, data)
        tx = self.finalize(tx, tx_format)
        return tx


    @classmethod
    def parse_give_to_request(cls, v):
        v = strip_0x(v)
        cursor = 0
        enc = ABIContractEncoder()
        enc.method('giveTo')
        enc.typ(ABIContractType.ADDRESS)
        r = enc.get()
        l = len(r)
        m = v[:l]
        if m != r:
            logg.error('method mismatch, expected {}, got {}'.format(r, m))
            raise RequestMismatchException(v)
        cursor += l

        dec = ABIContractDecoder()
        dec.typ(ABIContractType.ADDRESS)
        dec.val(v[cursor:cursor+64])
        r = dec.decode()
        return r
