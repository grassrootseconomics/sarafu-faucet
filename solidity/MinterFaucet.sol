pragma solidity > 0.6.11;

// SPDX-License-Identifier: GPL-3.0-or-later

contract MinterFaucet {
	// Implements EIP 173
	address public owner;
	address newOwner;
	// Implements Faucet
	address public token;
	address public accountsIndex;
	address store;
	// Implements Faucet
	uint256 public tokenAmount;
	address public overrider;
	uint256 public constant cooldownDisabled = 0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff;

	event FaucetUsed(address indexed _recipient, address indexed _token, uint256 _value);
	event FaucetFail(address indexed _recipient, address indexed _token, uint256 _value);
	event FaucetAmountChange(uint256 _value);
	event OwnershipTaken(address _result);
	event OwnershipTransferred(address indexed previousOwner, address indexed newOwner); // EIP173

	constructor(address _overrider, address _mintableToken, address _store, address _accountsIndex) public {
		owner = msg.sender;
		accountsIndex = _accountsIndex;
		store = _store;
		overrider = _overrider;
		token = _mintableToken;
	}

	// Implements OwnedTaker
	function takeOwnership(address _ownable) public returns (bool) {
		bool _ok;
		bytes memory _result;

		(_ok, _result) = _ownable.call(abi.encodeWithSignature("acceptOwnership()"));
		if (!_ok) {
			revert('ERR_TAKE_OWNERSHIP');
		}
		emit OwnershipTaken(_ownable);
		return true;
	}

	// Implements Faucet
	function setAmount(uint256 _amount) public returns (bool) {
		require(msg.sender == overrider);
		tokenAmount = _amount;
		emit FaucetAmountChange(_amount);
		return true;
	}

	// Implements Faucet
	function giveTo(address _recipient) public returns (bool) {
		bool ok;
		bytes memory result;
		bool result_parsed;

		if (accountsIndex != address(0)) {	
			(ok,  result) = accountsIndex.call(abi.encodeWithSignature("have(address)", _recipient));
			require(ok, 'ERR_ACCOUNT_INDEX_REVERT');
			require(result[31] != 0, 'ERR_ACCOUNT_INDEX');
		}

		(ok, result) = store.call(abi.encodeWithSignature("have(address)", _recipient));
		require(ok, 'ERR_ACCOUNT_HAVE_REVERT');
		result_parsed = abi.decode(result, (bool));
		require(result[31] == 0, 'ERR_ACCOUNT_HAVE_USED'); // less conversion than: // require(abi.decode(result, (bool)) == false, 'ERR_ACCOUNT_USED');
		require(!result_parsed, 'ERR_ACCOUNT_HAVE_USED'); // less conversion than: // require(abi.decode(result, (bool)) == false, 'ERR_ACCOUNT_USED');

		(ok, result) = store.call(abi.encodeWithSignature("add(address)", _recipient));
		result_parsed = abi.decode(result, (bool));
		require(ok, 'ERR_MARK_REVERT');
		require(result_parsed, 'ERR_ACCOUNT_ADD'); // less conversion than: // require(abi.decode(result, (bool)) == false, 'ERR_ACCOUNT_USED');

		(ok, result) = token.call(abi.encodeWithSignature("mintTo(address,uint256)", _recipient, tokenAmount)); 
		if (!ok) {
			emit FaucetFail(_recipient, token, tokenAmount);
			revert('ERR_MINT_REVERT');
		}
		if (result[31] == 0x00) {
			emit FaucetFail(_recipient, token, tokenAmount);
			revert('ERR_MINT');
		}
			
		emit FaucetUsed(_recipient, token, tokenAmount);
		return true;
	}

	// Implements EIP173
	function transferOwnership(address _newOwner) public returns (bool) {
		require(msg.sender == owner);
		newOwner = _newOwner;
	}

	// Implements OwnedAccepter
	function acceptOwnership() public returns (bool) {
		address oldOwner;

		require(msg.sender == newOwner);
		oldOwner = owner; 
		owner = newOwner;
		newOwner = address(0);
		emit OwnershipTransferred(oldOwner, owner);
	}

	// Implements Faucet	
	function cooldown(address _recipient) public returns (uint256) {
		bool ok;
		bytes memory result;
		bool result_parsed;

		(ok, result) = store.call(abi.encodeWithSignature("have(address)", _recipient));
		result_parsed = abi.decode(result, (bool));
		if (result_parsed) {
			return cooldownDisabled;
		} else {
			return 0;
		}
	}

	// Implements EIP165
	function supportsInterface(bytes4 _sum) public pure returns (bool) {
		if (_sum == 0xde344547) { // Faucet
			return true;
		}
		if (_sum == 0x01ffc9a7) { // EIP165
			return true;
		}
		if (_sum == 0x9493f8b2) { // EIP173
			return true;
		}
		if (_sum == 0x6b578339) { // OwnedTaker
			return true;
		}
		if (_sum == 0x37a47be4) { // OwnedAccepter
			return true;
		}
		return false;
	}
}
