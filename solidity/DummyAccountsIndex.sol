pragma solidity > 0.6.11;

// SPDX-License-Identifier: GPL-3.0-or-later

contract DummyAccountsIndex {
	mapping ( address => bool ) index;

	function have(address _address) public view returns (bool) {
		return index[_address];
	}

	function add(address _address) public returns (bool) {
		index[_address] = true;
		return true;
	}
}
